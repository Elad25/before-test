<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Category;



/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    
	<?php if($model->isNewRecord){ ?>  <!-- show password field only if isNewRecord activate  --->
    <?= $form->field ($model, 'password')->passwordInput(['maxlength' => true]) ?>
	<?php }?>   <!-- end of isNewRecord condition   -->

	<!-- unshow this field
    <?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
	
		<?php if(!$model->isNewRecord){ ?>  <!-- show password field only if isNewRecord activate  --->
	<?php if (\Yii::$app->user->can('createUser')) { //hide this button from who is not authorized => only the admin can!!! ?>
		<?= $form->field($model, 'role')->dropDownList($roles) ?>
		<? $cat=Category::getcategorys() ; //// for the dropdown?> 
		<?= $form->field($model, 'CategoryId')->dropDownList($cat) // Dropdown using another table, CategoryId is the attribute name, don't forget to add "use..." ?>	
	
	<?php } ?>
		<?php }?>   <!-- end of isNewRecord condition   -->
	
 <!-- unshow this fields
	<?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	  <?php //var_dump(User::getRoles())  ?>	

</div>
