<?php

use yii\helpers\Html;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update User: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roles' => $roles,
		'CategoryId' => $cat, /////// We need to add this when we want to add attribute/// don't forget to add attributeLabels and rules in user.php 
							///// cat is the dropdown at _form.php
    ]) ?>

</div>
