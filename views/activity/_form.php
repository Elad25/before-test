<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use app\models\User;
use app\models\Status;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'categoryId')->
				dropDownList(Category::getCategorys()) ?>	
   
	<?php if(!$model->isNewRecord){ //show owner field only if isNewRecord activate => if we don't try to update lead, only creating a new one ?>
	<?= $form->field($model, 'statusId')->
				dropDownList(Status::getStatuses()) ?>
	<?php } //end of isNewRecord activate ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
