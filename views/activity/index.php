<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'categoryId',
			[
				'attribute' => 'categoryId',
				'label' => 'Category Id',
				'format' => 'raw',
				//'value' => function($model){
				//	return array_keys(\Yii::$app->authManager->getRolesByUser($model->id))[0];					
				//},
				'filter'=>Html::dropDownList('ActivitySearch[categoryId]', $category, $categorys, ['class'=>'form-control']),
			],		
            'statusId',

            ['class' => 'yii\grid\ActionColumn'],
			
        ],
    ]); ?>
</div>
