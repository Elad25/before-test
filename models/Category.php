<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 20],
        ];
    }

	public static function getCategorys()  // This function are already exist 
	{
		$allCategorys = self::find()->all();
		$allCategorysArray = ArrayHelper::
					map($allCategorys, 'id', 'name');
		return $allCategorysArray;						
	}
	
	public static function getCategorysWithAllCategorys() //This new function are using the getStatuses function
	{
		$categorys = self::getCategorys();
		$categorys[-1] = 'All Categorys'; //We need to declare $status[-1] on model/LeadSearch -> only 1 row
		$categorys = array_reverse ( $categorys, true );
		return $categorys;	
	}	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
