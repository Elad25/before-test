<?php

use yii\db\Migration;

class m160514_172652_user_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'user',
		[
			'id' => 'pk',
			'username' => 'string',
			'password' => 'string',
			'auth_key' => 'string'
		],
		'ENGINE=InnoDB'
		);
    }
    public function down()
    {
       $this->dropTable('user') ;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
