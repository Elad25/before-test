<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->homeUrl = ['site/index'];	
		Yii::$app->user->logout();

		return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$user = $auth->createRole('user');
		$auth->add($user);
		
		$categoryLeader = $auth->createRole('categoryLeader');
		$auth->add($categoryLeader);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionUserpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createUser = $auth->createPermission('createUser');///////////
		$createUser->description = 'User can create new users';
		$auth->add($createUser);
		
		$createActivity = $auth->createPermission('createActivity'); ////////////
		$createActivity->description = 'User can create new activity';
		$auth->add($createActivity);
		
		
		$indexActivity = $auth->createPermission('indexActivity');///////////////////////
		$indexActivity->description = 'All users can view activity';
		$auth->add($indexActivity);
		
		$viewActivity = $auth->createPermission('viewActivity');/////////////////
		$viewActivity->description = 'View Activity';
		$auth->add($viewActivity);

	}


	public function actionCategotyleaderpermissions()
	{
		$auth = Yii::$app->authManager;

		$updateOwnCategory = $auth->createPermission('updateOwnCategory'); /////////////////
		$updateOwnCategory->description = 'Category leader can update
									own category';
		$auth->add($updateOwnCategory);		
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updateUser = $auth->createPermission('updateUser'); //////////////////////////
		$updateUser->description = 'Admin can update all users';
		$auth->add($updateUser);

		$deleteUser = $auth->createPermission('deleteUser'); //////////////////
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);

		$updateCategory = $auth->createPermission('updateCategory');  ////////////////
		$updateCategory->description = 'Admin can update all category';
		$auth->add($updateCategory);
		
		
		$deleteActivity = $auth->createPermission('deleteActivity'); ///////////////
		$deleteActivity->description = 'Admin can delete activity';
		$auth->add($deleteActivity);
	
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$user = $auth->getRole('user'); /////////////////

		$indexActivity = $auth->getPermission('indexActivity'); //////////////////////
		$auth->addChild($user, $indexActivity);

		$viewActivity = $auth->getPermission('viewActivity'); /////////////////////
		$auth->addChild($user, $viewActivity);

		$createActivity = $auth->getPermission('createActivity'); /////////////////////////
		$auth->addChild($user, $createActivity);

		$createUser = $auth->getPermission('createUser'); //////////////////////
		$auth->addChild($user, $createUser);		
		
		$categoryLeader = $auth->getRole('categoryLeader'); //////////////////////
		$auth->addChild($categoryLeader, $user);
		
		$updateOwnCategory = $auth->getPermission('updateOwnCategory'); ///////////////////
		$auth->addChild($categoryLeader, $updateOwnCategory);		
		
		$admin = $auth->getRole('admin'); /////////////////////
		$auth->addChild($admin, $categoryLeader);	
		
		$deleteUser = $auth->getPermission('deleteUser'); ////////////////////
		$auth->addChild($admin, $deleteUser);
		
		$updateUser = $auth->getPermission('updateUser'); /////////////////////
		$auth->addChild($admin, $updateUser);
		
		$updateCategory = $auth->getPermission('updateCategory'); /////////////
		$auth->addChild($admin, $updateCategory);

		$deleteActivity = $auth->getPermission('deleteActivity'); /////////////
		$auth->addChild($admin, $deleteActivity);

	}
	
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;	
		
		$updateOwnCategory = $auth->getPermission('updateOwnCategory');
		$auth->remove($updateOwnCategory);
		
		$rule = new \app\rbac\OwnCategoryLeaderRule; //// File name
		$auth->add($rule);
				
		$updateOwnCategory->ruleName = $rule->name;		
		$auth->add($updateOwnCategory);	
	}
}
