<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		//access control
		if (!\Yii::$app->user->can('createUser')) // all users can create new users 
		throw new UnauthorizedHttpException ('Hey, You are not allowed to watch users');
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'roles' => User::getRolesWithAllRoles(), // Takes variables from model/status to the dropDown in the lead/index ////////////////////////////
			'role' => $searchModel->role, // Takes variables from model/status to the dropDown in the lead/index /////////////////////////////////
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
 		//access control
		if (!\Yii::$app->user->can('createUser')) // all users can create new users 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create new users');
		$model = new User();
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles(); 
            return $this->render('create', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		//access control
		if (!\Yii::$app->user->can('updateUser') &&  //everyone can update their user profile but only admin can update every user
		    !\Yii::$app->user->can('updateOwnUser', ['user' =>$model]) )
			throw new UnauthorizedHttpException ('Hey, You are not allowed to update users'); 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles(); 
			return $this->render('update', [
				'model' => $model,
				'roles' => $roles,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		//access control
		if (!\Yii::$app->user->can('deleteUser')) // only Admin can delete users 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to delete new users');
			$this->findModel($id)->delete();
				return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
			$roles = \Yii::$app->authManager->getRolesByUser($model->id);
			$model->role = array_keys($roles)[0];
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
